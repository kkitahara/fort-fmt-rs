// Copyright 2020 Koichi Kitahara
//
// Licensed under either of Apache License, Version 2.0:
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// or MIT license:
//
//   Permission is hereby granted, free of charge, to any person obtaining a
//   copy of this software and associated documentation files (the "Software"),
//   to deal in the Software without restriction, including without limitation
//   the rights to use, copy, modify, merge, publish, distribute, sublicense,
//   and/or sell copies of the Software, and to permit persons to whom the
//   Software is furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in
//   all copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//   DEALINGS IN THE SOFTWARE.
//
// at your option.

mod fmt;

pub use fmt::{FmtItem, FmtSpec};

use core::convert::TryFrom;
use core::iter::repeat;
use core::num::NonZeroUsize;
use lazy_static::lazy_static;
use regex::Regex;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Seek};
use std::path::Path;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Blank {
    Null,
    Zero,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Decimal {
    Point,
    Comma,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Pad {
    Yes,
    No,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum State {
    Advancing,
    Nonadvancing,
    EndOfRecord,
    AfterEndfileRecord,
}

// TODO: implement rounding mode?
pub struct SequentialReader<R> {
    reader: R,
    blank: Blank,
    decimal: Decimal,
    pad: Pad,
    state: State,
    record: String,
    len_char: usize,
    pos_char: usize,
    field: String,
}

impl SequentialReader<BufReader<File>> {
    pub fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let f = File::open(path)?;
        Ok(Self::from_reader(BufReader::new(f)))
    }
}

impl<R> SequentialReader<R>
where
    R: BufRead,
{
    pub fn from_reader(reader: R) -> Self {
        Self {
            reader,
            blank: Blank::Null,
            decimal: Decimal::Point,
            pad: Pad::Yes,
            state: State::Advancing,
            record: String::new(),
            len_char: 0,
            pos_char: 0,
            field: String::new(),
        }
    }

    pub fn rewind(&mut self) -> io::Result<()>
    where
        R: Seek,
    {
        self.reader.seek(io::SeekFrom::Start(0))?;
        self.state = State::Advancing;

        Ok(())
    }

    // TODO: implement backspace?
    // * need to know the position 0 of the previous record(s)
    // * need to know if a record is written by list-directed or namelist

    pub fn blank(&self) -> Blank {
        self.blank
    }

    pub fn set_blank(&mut self, blank: Blank) {
        self.blank = blank;
    }

    pub fn decimal(&self) -> Decimal {
        self.decimal
    }

    pub fn set_decimal(&mut self, decimal: Decimal) {
        self.decimal = decimal;
    }

    pub fn pad(&self) -> Pad {
        self.pad
    }

    pub fn set_pad(&mut self, pad: Pad) {
        self.pad = pad;
    }

    // advancing input
    pub fn read_record(&mut self, pad: Option<Pad>) -> io::Result<Record<R>> {
        match self.state {
            State::Advancing => {
                self.set_next_record()?;
                Ok(Record::new(self, pad))
            }
            State::Nonadvancing => {
                self.state = State::Advancing;
                Ok(Record::new(self, pad))
            }
            State::EndOfRecord => {
                self.state = State::Advancing;
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "end of record in last input",
                ))
            }
            State::AfterEndfileRecord => Err(io::Error::new(
                io::ErrorKind::Other,
                "read after endfile record",
            )),
        }
    }

    // nonadvancing input
    pub fn nonadvancing_read_record(&mut self, pad: Option<Pad>) -> io::Result<Record<R>> {
        match self.state {
            State::Advancing => {
                self.set_next_record()?;
                self.state = State::Nonadvancing;
                Ok(Record::new(self, pad))
            }
            State::Nonadvancing => Ok(Record::new(self, pad)),
            State::EndOfRecord => {
                self.state = State::Advancing;
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "end of record in last input",
                ))
            }
            State::AfterEndfileRecord => Err(io::Error::new(
                io::ErrorKind::Other,
                "read after endfile record",
            )),
        }
    }

    fn set_next_record(&mut self) -> io::Result<()> {
        self.record.clear();
        let len = self.reader.read_line(&mut self.record)?;
        if len == 0 {
            self.state = State::AfterEndfileRecord;
            Err(io::Error::new(io::ErrorKind::Other, "end of file"))
        } else {
            if self.record.ends_with('\x0a') {
                self.record.pop();
            }

            if self.record.ends_with('\x0d') {
                self.record.pop();
            }

            self.len_char = self.record.chars().count();
            self.pos_char = 0;

            Ok(())
        }
    }
}

pub struct Record<'a, R> {
    scale: i32,
    blank: Blank,
    decimal: Decimal,
    pad: Pad,
    left_tab_limit_char: usize,
    next_pos_char: usize,
    reader: &'a mut SequentialReader<R>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Sign {
    Plus,
    Minus,
    None,
}

lazy_static! {
    static ref STANDARD_FORM_INTEGER: Regex = Regex::new(r"^[+-]?[0-9]+$").unwrap();
    static ref STANDARD_FORM_LOGICAL: Regex = Regex::new(r"^\.?[TtFf]").unwrap();
    static ref STANDARD_FORM_INF: Regex =
        Regex::new(r"^[Ii][Nn][Ff](?:[Ii][Nn][Ii][Tt][Yy])?\x20*$").unwrap();
    static ref STANDARD_FORM_NAN: Regex =
        Regex::new(r"^[Nn][Aa][Nn](?:\([0-9a-zA-Z]*\))?\x20*$").unwrap();
    static ref FLOAT_SEPARATOR: Regex = Regex::new(r"[EeDd+-]").unwrap();
    static ref STANDARD_FORM_FLOAT_SIGNIFICAND: Regex =
        Regex::new(r"^(?:[0-9]+\.[0-9]*|\.[0-9]+)$").unwrap();
    static ref STANDARD_FORM_FLOAT_NO_DECIMAL: Regex = Regex::new(r"^[0-9]+$").unwrap();
    static ref STANDARD_FORM_FLOAT_EXPONENT: Regex =
        Regex::new(r"^(?:[EeDd][+-]?|[+-])[0-9]+$").unwrap();
}

macro_rules! iw {
    ($iw: ident, $T: ty, $err: expr) => {
        pub fn $iw(&mut self, w: NonZeroUsize) -> io::Result<$T> {
            self.check_eor(w)?;
            self.prepare_field_for_int_or_bool(w);

            let s = &self.reader.field;

            if s.is_empty() {
                Ok(0)
            } else if STANDARD_FORM_INTEGER.is_match(s) {
                s.parse().map_err(|_| $err)
            } else {
                Err($err)
            }
        }
    };
}

macro_rules! fwd {
    ($fwd: ident, $T: ty, $err: expr) => {
        pub fn $fwd(&mut self, w: NonZeroUsize, d: usize) -> io::Result<$T> {
            self.check_eor(w)?;
            let sign = self.prepare_field_for_float(w);

            let s = &self.reader.field;

            if sign == Sign::None && s.is_empty() {
                Ok(0.0)
            } else if STANDARD_FORM_INF.is_match(s) {
                match sign {
                    Sign::Plus | Sign::None => Ok(<$T>::INFINITY),
                    Sign::Minus => Ok(<$T>::NEG_INFINITY),
                }
            } else if STANDARD_FORM_NAN.is_match(s) {
                match sign {
                    Sign::Plus | Sign::None => Ok(<$T>::NAN),
                    Sign::Minus => Ok(-<$T>::NAN),
                }
            } else {
                let (significand, exponent) = match FLOAT_SEPARATOR.shortest_match(s) {
                    Some(end) if end > 0 => s.split_at(end - 1),
                    _ => s.split_at(s.len()),
                };

                let mut exponent = if exponent.is_empty() {
                    -self.scale
                } else if STANDARD_FORM_FLOAT_EXPONENT.is_match(exponent) {
                    exponent.parse().map_err(|_| $err)?
                } else {
                    Err($err)?
                };

                let significand: $T = if STANDARD_FORM_FLOAT_SIGNIFICAND.is_match(significand) {
                    significand.parse().map_err(|_| $err)?
                } else if STANDARD_FORM_FLOAT_NO_DECIMAL.is_match(significand) {
                    exponent = exponent
                        .checked_sub(i32::try_from(d).expect("d should not overflow i32"))
                        .expect("exponent - d should not overflow i32");
                    significand.parse().map_err(|_| $err)?
                } else {
                    Err($err)?
                };

                match sign {
                    Sign::Plus | Sign::None => Ok(significand * <$T>::powi(10.0, exponent)),
                    Sign::Minus => Ok(-significand * <$T>::powi(10.0, exponent)),
                }
            }
        }
    };
}

impl<'a, R> Record<'a, R>
where
    R: BufRead,
{
    fn new(reader: &'a mut SequentialReader<R>, pad: Option<Pad>) -> Self {
        Self {
            scale: 0,
            blank: reader.blank(),
            decimal: reader.decimal(),
            pad: pad.unwrap_or(reader.pad()),
            left_tab_limit_char: reader.pos_char,
            next_pos_char: reader.pos_char,
            reader,
        }
    }

    fn check_eor(&mut self, w: NonZeroUsize) -> io::Result<()> {
        if self.reader.state == State::EndOfRecord {
            // eor condition has already been caught
            Ok(())
        } else {
            let w = w.get();
            let rem = self.reader.len_char.saturating_sub(self.next_pos_char);
            let eor = rem < w;

            if eor && self.pad == Pad::No {
                Err(io::Error::new(io::ErrorKind::Other, "end of record"))
            } else if eor && self.reader.state == State::Nonadvancing {
                // catch eor condition
                self.reader.state = State::EndOfRecord;
                Ok(())
            } else {
                Ok(())
            }
        }
    }

    fn prepare_field(&mut self, w: NonZeroUsize) {
        let w = w.get();

        let iter = self
            .reader
            .record
            .chars()
            .skip(self.next_pos_char)
            .chain(repeat(' '))
            .take(w);

        let field = &mut self.reader.field;
        field.clear();
        field.extend(iter);
    }

    pub fn aw_str(&mut self, w: NonZeroUsize) -> io::Result<&str> {
        self.check_eor(w)?;
        self.prepare_field(w);

        let s = &self.reader.field;

        Ok(s)
    }

    fn prepare_field_for_int_or_bool(&mut self, w: NonZeroUsize) {
        let w = w.get();
        let blank = self.blank;

        let iter = self
            .reader
            .record
            .chars()
            .skip(self.next_pos_char)
            .chain(repeat(' '))
            .take(w)
            .skip_while(|c| *c == ' ')
            .filter_map(|c| match (c, blank) {
                (' ', Blank::Zero) => Some('0'),
                (' ', Blank::Null) => None,
                _ => Some(c),
            });

        let field = &mut self.reader.field;
        field.clear();
        field.extend(iter);
    }

    iw! {iw_i8, i8, io::Error::new(io::ErrorKind::Other, "failed to parse i8")}
    iw! {iw_i16, i16, io::Error::new(io::ErrorKind::Other, "failed to parse i16")}
    iw! {iw_i32, i32, io::Error::new(io::ErrorKind::Other, "failed to parse i32")}
    iw! {iw_i64, i64, io::Error::new(io::ErrorKind::Other, "failed to parse i64")}
    iw! {iw_i128, i128, io::Error::new(io::ErrorKind::Other, "failed to parse i128")}
    iw! {iw_isize, isize, io::Error::new(io::ErrorKind::Other, "failed to parse isize")}

    pub fn lw_bool(&mut self, w: NonZeroUsize) -> io::Result<bool> {
        self.check_eor(w)?;
        self.prepare_field_for_int_or_bool(w);

        let s = &self.reader.field;

        match STANDARD_FORM_LOGICAL.shortest_match(s) {
            Some(end) if end > 0 => Ok(s[..end].ends_with(&['T', 't'][..])),
            _ => Err(io::Error::new(io::ErrorKind::Other, "failed to parse bool")),
        }
    }

    fn prepare_field_for_float(&mut self, w: NonZeroUsize) -> Sign {
        let w = w.get();
        let blank = self.blank;
        let decimal = self.decimal;

        let mut iter = self
            .reader
            .record
            .chars()
            .skip(self.next_pos_char)
            .chain(repeat(' '))
            .take(w)
            .skip_while(|c| *c == ' ')
            .peekable();

        let sign = match iter.peek() {
            Some('+') => {
                iter.next();
                Sign::Plus
            }
            Some('-') => {
                iter.next();
                Sign::Minus
            }
            _ => Sign::None,
        };

        match iter.peek() {
            Some('I') | Some('i') | Some('N') | Some('n') => {
                // Inf or NaN
                let field = &mut self.reader.field;
                field.clear();
                field.extend(iter);
            }
            _ => {
                let iter = iter
                    .scan(false, |after_exp_symbol, c| {
                        match (*after_exp_symbol, c, blank, decimal) {
                            (true, ' ', _, _) => Some(c),
                            (true, _, _, _) => {
                                *after_exp_symbol = false;
                                Some(c)
                            }
                            (false, 'E', _, _)
                            | (false, 'e', _, _)
                            | (false, 'D', _, _)
                            | (false, 'd', _, _) => {
                                *after_exp_symbol = true;
                                Some(c)
                            }
                            (false, ' ', Blank::Zero, _) => Some('0'),
                            (false, ',', _, Decimal::Comma) => Some('.'),
                            (false, _, _, _) => Some(c),
                        }
                    })
                    .filter(|c| *c != ' ');

                let field = &mut self.reader.field;
                field.clear();
                field.extend(iter);
            }
        }

        sign
    }

    fwd! {fwd_f32, f32, io::Error::new(io::ErrorKind::Other, "failed to parse f32")}
    fwd! {fwd_f64, f64, io::Error::new(io::ErrorKind::Other, "failed to parse f64")}

    // TODO: pub fn bw(&mut self, w: NonZeroUsize) {}
    // TODO: pub fn ow(&mut self, w: NonZeroUsize) {}
    // TODO: pub fn zw(&mut self, w: NonZeroUsize) {}
    //
    // These are not portable because the imterpretation of these input is processer dependent

    pub fn gw_str(&mut self, w: NonZeroUsize) -> io::Result<&str> {
        self.aw_str(w)
    }

    pub fn gw_i8(&mut self, w: NonZeroUsize) -> io::Result<i8> {
        self.iw_i8(w)
    }

    pub fn gw_i16(&mut self, w: NonZeroUsize) -> io::Result<i16> {
        self.iw_i16(w)
    }

    pub fn gw_i32(&mut self, w: NonZeroUsize) -> io::Result<i32> {
        self.iw_i32(w)
    }

    pub fn gw_i64(&mut self, w: NonZeroUsize) -> io::Result<i64> {
        self.iw_i64(w)
    }

    pub fn gw_i128(&mut self, w: NonZeroUsize) -> io::Result<i128> {
        self.iw_i128(w)
    }

    pub fn gw_isize(&mut self, w: NonZeroUsize) -> io::Result<isize> {
        self.iw_isize(w)
    }

    pub fn gw_bool(&mut self, w: NonZeroUsize) -> io::Result<bool> {
        self.lw_bool(w)
    }

    pub fn gwd_f32(&mut self, w: NonZeroUsize, d: usize) -> io::Result<f32> {
        self.fwd_f32(w, d)
    }

    pub fn gwd_f64(&mut self, w: NonZeroUsize, d: usize) -> io::Result<f64> {
        self.fwd_f64(w, d)
    }

    // TODO: pub fn dt(&mut self) {}

    pub fn t(&mut self, n: NonZeroUsize) {
        if self.reader.state != State::EndOfRecord {
            let n = n.get();
            self.next_pos_char = self.left_tab_limit_char + (n - 1);
        }
    }

    pub fn tl(&mut self, n: NonZeroUsize) {
        if self.reader.state != State::EndOfRecord {
            let n = n.get();
            self.next_pos_char = self
                .reader
                .pos_char
                .saturating_sub(n)
                .max(self.left_tab_limit_char);
        }
    }

    // panics if n + pos overflows
    pub fn tr(&mut self, n: NonZeroUsize) {
        if self.reader.state != State::EndOfRecord {
            let n = n.get();
            self.next_pos_char = self
                .reader
                .pos_char
                .checked_add(n)
                .expect("pos + n should not overflow usize");
        }
    }

    // panics if n + pos overflows
    pub fn x(&mut self, n: NonZeroUsize) {
        if self.reader.state != State::EndOfRecord {
            self.tr(n);
        }
    }

    pub fn slash(&mut self) -> io::Result<()> {
        if self.reader.state != State::EndOfRecord {
            self.reader.set_next_record()?;

            self.left_tab_limit_char = 0;
            self.next_pos_char = 0;

            Ok(())
        } else {
            Ok(())
        }
    }

    pub fn p(&mut self, k: i32) {
        if self.reader.state != State::EndOfRecord {
            self.scale = k;
        }
    }

    pub fn bn(&mut self) {
        if self.reader.state != State::EndOfRecord {
            self.blank = Blank::Null;
        }
    }

    pub fn bz(&mut self) {
        if self.reader.state != State::EndOfRecord {
            self.blank = Blank::Zero;
        }
    }

    pub fn dp(&mut self) {
        if self.reader.state != State::EndOfRecord {
            self.decimal = Decimal::Point;
        }
    }

    pub fn dc(&mut self) {
        if self.reader.state != State::EndOfRecord {
            self.decimal = Decimal::Comma;
        }
    }
}
