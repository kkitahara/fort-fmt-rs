// Copyright 2020 Koichi Kitahara
//
// Licensed under either of Apache License, Version 2.0:
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// or MIT license:
//
//   Permission is hereby granted, free of charge, to any person obtaining a
//   copy of this software and associated documentation files (the "Software"),
//   to deal in the Software without restriction, including without limitation
//   the rights to use, copy, modify, merge, publish, distribute, sublicense,
//   and/or sell copies of the Software, and to permit persons to whom the
//   Software is furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in
//   all copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//   DEALINGS IN THE SOFTWARE.
//
// at your option.

// NOTE:
// * `r` shall be positive
// * `w` shall be positive (for input)
// * `len` shall be positive
//
// Only uppercase is supported for format items
//
// For input F, E, En, Es, and D are identical, only F is supported

#[derive(Debug, PartialEq, Clone)]
pub enum FmtItem {
    Unlimited { len: usize },
    Group { r: usize, len: usize },
    I { r: usize, w: usize },
    F { r: usize, w: usize, d: usize },
    G { r: usize, w: usize, d: usize },
    L { r: usize, w: usize },
    A { r: usize, w: usize },
    T { n: usize },
    Tl { n: usize },
    Tr { n: usize },
    X { n: usize },
    Slash { r: usize },
    Colon,
    P { k: i32 },
    Bn,
    Bz,
    Dc,
    Dp,
}

#[derive(Debug, PartialEq)]
pub struct FmtSpec<T> {
    fmt_items: T,
    max_depth: usize,
}

// depth_flag:
// - () (plus)
// - [] (minus)
#[doc(hidden)]
#[macro_export]
macro_rules! max_depth {
    // top level termianl
    (() => ($max_depth:tt, $relative_depth:tt) | []) => {
        <[()]>::len(&$max_depth)
    };
    // lower level terminal (deeper than previous max)
    (() => ([$($max_depth_flag:tt)*], [(), $($relative_depth_flag:tt)*]) | [$parent:tt $($stack:tt)*]) => {
        $crate::max_depth!($parent => ([$($max_depth_flag)* (), $($relative_depth_flag)*], [[],]) | [$($stack)*])
    };
    // lower level terminal (not deeper than previous max)
    (() => ($max_depth:tt, [$($relative_depth_flag:tt)*]) | [$parent:tt $($stack:tt)*]) => {
        $crate::max_depth!($parent => ($max_depth, [[], $($relative_depth_flag)*]) | [$($stack)*])
    };
    // new group where relative depth is negative
    ((($($inner:tt)+) $($tail:tt)*) => ($max_depth:tt, [[], $($relative_depth_flag:tt)*]) | [$($stack:tt)*]) => {
        $crate::max_depth!(($($inner)+) => ($max_depth, [$($relative_depth_flag)*]) | [($($tail:tt)*) $($stack)*])
    };
    // new group where relative depth is not negative
    ((($($inner:tt)+) $($tail:tt)*) => ($max_depth:tt, [$($relative_depth_flag:tt)*]) | [$($stack:tt)*]) => {
        $crate::max_depth!(($($inner)+) => ($max_depth, [(), $($relative_depth_flag)*]) | [($($tail:tt)*) $($stack)*])
    };
    // non-group token
    (($nongroup:tt $($tail:tt)*) => $depth:tt | $stack:tt) => {
        $crate::max_depth!(($($tail)*) => $depth | $stack)
    };
}

// [context: @first | @optional_comma | @after_p]? (tt) => [fmt token] | (depth flag: +) [(stack), ...]
#[doc(hidden)]
#[macro_export]
macro_rules! input_fmt_items {
    // empty
    (@first () => [$($body:tt)*] | ($($depth:tt)*) []) => {
        [$($body)*]
    };
    (@optional_comma () => [$($body:tt)+] | ($($depth:tt)*) []) => {
        [$($body)+]
    };
    ($(@after_p)? () => [$($body:tt)+] | ($($depth:tt)*) []) => {
        [$($body)+]
    };
    (@optional_comma () => [$($body:tt)+] | (+ $($depth:tt)*) [$parent:tt $($stack:tt)*]) => {
        $crate::input_fmt_items!($parent => [$($body)+] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? () => [$($body:tt)+] | (+ $($depth:tt)*) [$parent:tt $($stack:tt)*]) => {
        $crate::input_fmt_items!($parent => [$($body)+] | ($($depth)*) [$($stack)*])
    };

    // * (...)
    (@first (*($($inner:tt)+)) => [] | () []) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$crate::input::FmtItem::Unlimited {
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+) [])
    };
    (@optional_comma ($(,)? *($($inner:tt)+)) => [$($body:tt)+] | () []) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Unlimited {
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+) [])
    };
    ($(@after_p)? (, *($($inner:tt)+)) => [$($body:tt)+] | () []) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Unlimited {
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+) [])
    };

    // [r] (...)
    (@first (($($inner:tt)+) $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)* $crate::input::FmtItem::Group {
            r: 1,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };
    (@first ($r:literal ($($inner:tt)+) $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)* $crate::input::FmtItem::Group {
            r: $r,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };
    (@optional_comma ($(,)? ($($inner:tt)+) $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Group {
            r: 1,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };
    (@optional_comma ($(,)? $r:literal ($($inner:tt)+) $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Group {
            r: $r,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };
    ($(@after_p)? (, ($($inner:tt)+) $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Group{
            r: 1,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };
    ($(@after_p)? (, $r:literal ($($inner:tt)+) $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@first ($($inner)+) => [$($body)+ $crate::input::FmtItem::Group{
            r: $r,
            len: <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($inner)+) => [] | (+) []))
        },] | (+ $($depth)*) [($($tail)*) $($stack)*])
    };

    // [r] I@w
    (@first (I@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::I { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal I@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::I { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? I@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::I { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal I@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::I { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, I@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::I { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $r:literal I@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::I { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };

    // [r] F@w@d
    (@first (F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::F { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::F { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@after_p ($(,)? F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@after_p ($(,)? $r:literal F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    ((, F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    ((, $r:literal F@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::F { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };

    // [r] G@w@d
    (@first (G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::G { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::G { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@after_p ($(,)? G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    (@after_p ($(,)? $r:literal G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    ((, G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: 1, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };
    ((, $r:literal G@$w:literal@$d:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::G { r: $r, w: $w, d: $d },] | ($($depth)*) [$($stack)*])
    };

    // [r] L@w
    (@first (L@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::L { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal L@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::L { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? L@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::L { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal L@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::L { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, L@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::L { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $r:literal L@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::L { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };

    // [r] A@w
    (@first (A@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::A { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal A@$w:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::A { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? A@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::A { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal A@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::A { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, A@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::A { r: 1, w: $w },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $r:literal A@$w:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::A { r: $r, w: $w },] | ($($depth)*) [$($stack)*])
    };

    // T n
    (@first (T $n:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::T { n: $n },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? T $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::T { n: $n },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, T $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::T { n: $n },] | ($($depth)*) [$($stack)*])
    };

    // TL n
    (@first (TL $n:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Tl { n: $n },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? TL $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Tl { n: $n },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, TL $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Tl { n: $n },] | ($($depth)*) [$($stack)*])
    };

    // TR n
    (@first (TR $n:literal $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Tr { n: $n },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? TR $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Tr { n: $n },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, TR $n:literal $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Tr { n: $n },] | ($($depth)*) [$($stack)*])
    };

    // n X
    (@first ($n:literal X $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::X { n: $n },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $n:literal X $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::X { n: $n },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $n:literal X $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::X { n: $n },] | ($($depth)*) [$($stack)*])
    };

    // [r] /
    (@first (/ $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)* $crate::input::FmtItem::Slash { r: 1 },] | ($($depth)*) [$($stack)*])
    };
    (@first ($r:literal / $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)* $crate::input::FmtItem::Slash { r: $r },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? / $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Slash { r: 1 },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $r:literal / $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Slash { r: $r },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? ($(,)? / $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Slash { r: 1 },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $r:literal / $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Slash { r: $r },] | ($($depth)*) [$($stack)*])
    };

    // :
    (@first (: $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)* $crate::input::FmtItem::Colon,] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? : $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Colon,] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? ($(,)? : $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@optional_comma ($($tail)*) => [$($body)+ $crate::input::FmtItem::Colon,] | ($($depth)*) [$($stack)*])
    };

    // k P
    (@first ($k:literal P $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@after_p ($($tail)*) => [$($body)* $crate::input::FmtItem::P { k: $k },] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? $k:literal P $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@after_p ($($tail)*) => [$($body)+ $crate::input::FmtItem::P { k: $k },] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, $k:literal P $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(@after_p ($($tail)*) => [$($body)+ $crate::input::FmtItem::P { k: $k },] | ($($depth)*) [$($stack)*])
    };

    // BN
    (@first (BN $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Bn,] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? BN $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Bn,] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, BN $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Bn,] | ($($depth)*) [$($stack)*])
    };

    // BZ
    (@first (BZ $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Bz,] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? BZ $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Bz,] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, BZ $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Bz,] | ($($depth)*) [$($stack)*])
    };

    // DC
    (@first (DC $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Dc,] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? DC $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Dc,] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, DC $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Dc,] | ($($depth)*) [$($stack)*])
    };

    // DP
    (@first (DP $($tail:tt)*) => [$($body:tt)*] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)* $crate::input::FmtItem::Dp,] | ($($depth)*) [$($stack)*])
    };
    (@optional_comma ($(,)? DP $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Dp,] | ($($depth)*) [$($stack)*])
    };
    ($(@after_p)? (, DP $($tail:tt)*) => [$($body:tt)+] | ($($depth:tt)*) [$($stack:tt)*]) => {
        $crate::input_fmt_items!(($($tail)*) => [$($body)+ $crate::input::FmtItem::Dp,] | ($($depth)*) [$($stack)*])
    };
}

#[macro_export]
macro_rules! input_fmt {
    ($($fmt_tt:tt)*) => {
        $crate::input::fmt::FmtSpec::<[$crate::input::FmtItem; <[$crate::input::FmtItem]>::len(&$crate::input_fmt_items!(@first ($($fmt_tt)*) => [] | () []))]> {
            fmt_items: $crate::input_fmt_items!(@first ($($fmt_tt)*) => [] | () []),
            max_depth: $crate::max_depth!(($($fmt_tt)*) => ([], []) | []),
        }
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_max_depth() {
        assert_eq!(max_depth!(() => ([], []) | []), 0);
        assert_eq!(max_depth!((6 I@1, TL 1) => ([], []) | []), 0);
        assert_eq!(max_depth!((I@31:2(I@1, 2 X)) => ([], []) | []), 1);
        assert_eq!(
            max_depth!((I@31/3 I@31, 2 I@1, TR 2, T 2) => ([], []) | []),
            0
        );
        assert_eq!(
            max_depth!((I@31, 3 I@31, 2 I@1, *(I@3, 2(I@2), I@1)) => ([], []) | []),
            2
        );
        assert_eq!(
            max_depth!((I@31, (3 I@31, ((I@1, I@1), I@1)), 2 I@1, *(I@3, 2(I@2), I@1)) => ([], []) | []),
            3
        );
    }

    #[test]
    fn test_input_fmt() {
        assert_eq!(input_fmt!().fmt_items.len(), 0);
        assert_eq!(input_fmt!(6 I@1, TL 1).fmt_items.len(), 2);
        assert_eq!(input_fmt!(I@31:2(I@1, 2 X)).fmt_items.len(), 5);
        assert_eq!(input_fmt!(I@31/3 I@31, 2 I@1, TR 2, T 2).fmt_items.len(), 6);
        assert_eq!(input_fmt!(I@31,3/3 I@31,-1 P, 2 I@1).fmt_items.len(), 5);
        assert_eq!(input_fmt!(I@31:3/3 I@31, 2 I@1).fmt_items.len(), 5);
        assert_eq!(
            input_fmt!(I@31, 3 I@31, 2 I@1, *(I@3, 2(I@2), I@1))
                .fmt_items
                .len(),
            8
        );
    }
}
